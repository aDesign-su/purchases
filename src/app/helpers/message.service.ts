import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

constructor(private _snackBar: MatSnackBar) { }

horizontalPosition: MatSnackBarHorizontalPosition = 'end';
public access: string = ''
public error: string = ''

openMessageBar(message: string, action: string, className: string) {
  this._snackBar.open(message, action, {
    duration: 5000,
    panelClass: [className],
    horizontalPosition: this.horizontalPosition,
  });
}

}
