import { Injectable } from '@angular/core';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

constructor(private location: Location) { }

  openDialog!: boolean
  position: any = 'right';
  isMaximized: boolean = false;

  toggleMaximize() {
    this.isMaximized = !this.isMaximized;
  }
  isOpen() {
    this.openDialog = !this.openDialog
  }
  goBack() {
    this.location.back();
  }

}
