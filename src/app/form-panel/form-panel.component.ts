import { Component } from "@angular/core";
import { DialogService } from "../helpers/dialog.service";


@Component({
    selector: 'app-form-panel',
    templateUrl: './form-panel.component.html',
    styleUrl: './form-panel.component.scss'
})
export class FormPanelComponent {

    constructor(public dialog: DialogService) {}

}