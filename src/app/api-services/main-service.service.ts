import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {
  public httpOptions: any;
  public httpFile: any;
  
  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('access'),
      })
    };
    this.httpFile = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + localStorage.getItem('access'),
      })
    };
  }
}
