import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, catchError, map, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from './../model/user';
import { Router } from '@angular/router';
import { MainServiceService } from './main-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // текущий JWT токен
  public token: any;
  // время окончания жизни токена
  public token_expires: any;
  // логин пользователя
  public username: any;
  // сообщения об ошибках авторизации
  public errors: any = [];

  constructor(private http: HttpClient, private router: Router, private option: MainServiceService) { }
    // Вход
    slogin(username: string, password: string): Observable<any> {
      const loginData = { username, password };
      return this.http.post<any>(`${environment.apiUrlAuth}/token/`, loginData)
        .pipe(
          map(response => { 
            localStorage.setItem('access', response.access);
            localStorage.setItem('refresh', response.refresh);
            this.updateData(response['refresh']);
            if (response.access) {
              this.router.navigate(['/home']);
            } else {
              this.router.navigate(['/login']);
            }
            return response;
          }),
          catchError((error: HttpErrorResponse) => {
            if (error.status === 401) {
              return throwError('Invalid credentials');
            } else {
              return throwError('Something went wrong');
            }
          })
        );
    }
  // Восстановление пароля
  resetPassword(email: string): Observable<any> {
    return this.http.post(`${environment.apiUrlAuth}/reset-password/send-mail/`, { email });
  }
  restorePassword(token: string, password: string, confirmed_password: string): Observable<any> {
    return this.http.post(`${environment.apiUrlAuth}/reset-password/verify-code/`, { token, password, confirmed_password });
  }
  // Изменение пароля
  changePassword(old_password: string, password: string, confirmed_password: string): Observable<any> {
    const payload = { old_password, password, confirmed_password };
    return this.http.put(`${environment.apiUrlAuth}/change-password/`, payload, this.option.httpOptions);
  }
  // обновление JWT токена
  public refreshToken() {
    this.http.post(`${environment.apiUrlAuth}/token/refresh/`, JSON.stringify({ token: this.token }), this.option.httpOptions.headers).subscribe(
      (data: any) => {
        this.updateData(data['refresh']);
      },
      err => {
        this.errors = err['error'];
      }
    );
  }
  public logout() {
    this.token = null;
    this.token_expires = null;
    this.username = null;
  }

  private updateData(access: any) {
    this.token = access;
    this.errors = [];

    // декодирование токена для получения логина и времени жизни токена
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.username = token_decoded.username;
  }
}
