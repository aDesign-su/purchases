import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Regions } from '../model/references/regions';
import { TreeNode } from 'primeng/api';
import { Observable, catchError, map, throwError } from 'rxjs';
import { Sbn } from '../model/references/sbn';
import { Spn } from '../model/references/spn';
import { CriticalityDelivery } from '../model/references/criticality-delivery';
import { PlaceDelivery } from '../model/references/place-delivery';
import { Tdbn } from '../model/references/tdbn';
import { DocumentOriginal } from '../model/references/document-original';
import { ApplicationStatus } from '../model/references/application-status';
import { DocumentOriginalStatus } from '../model/references/document-original-status';

@Injectable({
  providedIn: 'root'
})
export class ReferencesService {

constructor(private http: HttpClient) { }

/** Номенклатура НСИ */
getNomenclatureReference() {
  return this.http.get<TreeNode[]>(`${environment.apiUrl}/references/material_and_equip_class_tree/`)
  .pipe(
    map((data) => {
      return data[0].data;
  }),
    catchError((error) => {
      return throwError(error)
    })
  )
  /**
  .toPromise()
  .then(res => <TreeNode[]>res);
  */
}

/** Регионы */
  getRegionReference() {
    return this.http.get<Regions[]>(`${environment.apiUrl}/references/region/`);
  }

/** Статусы позиции потребности */
  getStatusesPositionNeed() {
    return this.http.get<Spn[]>(`${environment.apiUrl}/references/status_need_position/`);
  }
  addStatusesPositionNeed(data: Spn): Observable<Spn[]> {
    const url = `${environment.apiUrl}/references/status_need_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  editStatusesPositionNeed(id: number, data: any): Observable<Spn[]> {
    const url = `${environment.apiUrl}/references/status_need_update/${id}`;
    return this.http.put<Spn[]>(url, data);
  }
  delStatusesPositionNeed(id: number): Observable<Spn[]> {
    const url = `${environment.apiUrl}/references/status_need_update/${id}`;
    return this.http.delete<Spn[]>(url);
  }

/** Статусы основания потребности */
  getStatusesBasisNeed() {
    return this.http.get<Sbn[]>(`${environment.apiUrl}/references/status_need_reason/`);
  }
  addStatusesBasisNeed(data: Sbn): Observable<Sbn[]> {
    const url = `${environment.apiUrl}/references/status_need_reason_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  editStatusesBasisNeed(id: number, data: any): Observable<Sbn[]> {
    const url = `${environment.apiUrl}/references/status_need_reason_update/${id}`;
    return this.http.put<Sbn[]>(url, data);
  }
  delStatusesBasisNeed(id: number): Observable<Sbn[]> {
    const url = `${environment.apiUrl}/references/status_need_reason_update/${id}`;
    return this.http.delete<Sbn[]>(url);
  }

  /** Критичность поставки */
  getCriticalityDelivery() {
    return this.http.get<CriticalityDelivery[]>(`${environment.apiUrl}/references/criticality_of_delivery/`);
  }
  addCriticalityDelivery(data: CriticalityDelivery): Observable<CriticalityDelivery[]> {
    const url = `${environment.apiUrl}/references/criticality_of_delivery_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  editCriticalityDelivery(id: number, data: any): Observable<CriticalityDelivery[]> {
    const url = `${environment.apiUrl}/references/criticality_of_delivery_update/${id}`;
    return this.http.put<CriticalityDelivery[]>(url, data);
  }
  delCriticalityDelivery(id: number): Observable<CriticalityDelivery[]> {
    const url = `${environment.apiUrl}/references/criticality_of_delivery_update/${id}`;
    return this.http.delete<CriticalityDelivery[]>(url);
  }

  /** Место поставки */
  getPlaceDelivery() {
    return this.http.get<PlaceDelivery[]>(`${environment.apiUrl}/references/place_of_delivery/`);
  }
  addPlaceDelivery(data: PlaceDelivery): Observable<PlaceDelivery[]> {
    const url = `${environment.apiUrl}/references/place_of_delivery_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  editPlaceDelivery(id: number, data: any): Observable<PlaceDelivery[]> {
    const url = `${environment.apiUrl}/references/place_of_delivery_update/${id}`;
    return this.http.put<PlaceDelivery[]>(url, data);
  }
  delPlaceDelivery(id: number): Observable<PlaceDelivery[]> {
    const url = `${environment.apiUrl}/references/place_of_delivery_update/${id}`;
    return this.http.delete<PlaceDelivery[]>(url);
  }
  
  /** Типы документов основания потребности */
  getTypesDocumentsBasisNeed() {
    return this.http.get<Tdbn[]>(`${environment.apiUrl}/references/type_document_need_reason/`);
  }
  addTypesDocumentsBasisNeed(data: Tdbn): Observable<Tdbn[]> {
    const url = `${environment.apiUrl}/references/type_document_need_reason_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  editTypesDocumentsBasisNeed(id: number, data: any): Observable<Tdbn[]> {
    const url = `${environment.apiUrl}/references/type_document_need_reason_update/${id}`;
    return this.http.put<Tdbn[]>(url, data);
  }
  delTypesDocumentsBasisNeed(id: number): Observable<Tdbn[]> {
    const url = `${environment.apiUrl}/references/type_document_need_reason_update/${id}`;
    return this.http.delete<Tdbn[]>(url);
  }

  /** Типы документов исходных данных */
  getDocumentOriginal() {
    return this.http.get<DocumentOriginal[]>(`${environment.apiUrl}/references/document_original_data_types/`);
  }
  addDocumentOriginal(data: DocumentOriginal): Observable<DocumentOriginal[]> {
    const url = `${environment.apiUrl}/references/document_original_data_types_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  ediDtocumentOriginal(id: number, data: any): Observable<DocumentOriginal[]> {
    const url = `${environment.apiUrl}/references/document_original_data_types_update/${id}`;
    return this.http.put<DocumentOriginal[]>(url, data);
  }
  delDocumentOriginal(id: number): Observable<DocumentOriginal[]> {
    const url = `${environment.apiUrl}/references/document_original_data_types_update/${id}`;
    return this.http.delete<DocumentOriginal[]>(url);
  }

  /** Статусы заявок */
  getApplicationStatus() {
    return this.http.get<ApplicationStatus[]>(`${environment.apiUrl}/references/application_status/`);
  }
  addApplicationStatus(data: ApplicationStatus): Observable<ApplicationStatus[]> {
    const url = `${environment.apiUrl}/references/application_status_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  ediApplicationStatus(id: number, data: any): Observable<ApplicationStatus[]> {
    const url = `${environment.apiUrl}/references/application_status_update/${id}`;
    return this.http.put<ApplicationStatus[]>(url, data);
  }
  delApplicationStatus(id: number): Observable<ApplicationStatus[]> {
    const url = `${environment.apiUrl}/references/application_status_update/${id}`;
    return this.http.delete<ApplicationStatus[]>(url);
  }

  /** Статус документов исходных данных */
  getDocumentOriginalStatus() {
    return this.http.get<DocumentOriginalStatus[]>(`${environment.apiUrl}/references/application_status/`);
  }
  addDocumentOriginalStatus(data: DocumentOriginalStatus): Observable<DocumentOriginalStatus[]> {
    const url = `${environment.apiUrl}/references/application_status_create/`;
    return this.http.post(url, data).pipe(
      map((response: any) => response.body)
    );
  }
  ediDocumentOriginalStatus(id: number, data: any): Observable<DocumentOriginalStatus[]> {
    const url = `${environment.apiUrl}/references/application_status_update/${id}`;
    return this.http.put<DocumentOriginalStatus[]>(url, data);
  }
  delDocumentOriginalStatus(id: number): Observable<DocumentOriginalStatus[]> {
    const url = `${environment.apiUrl}/references/application_status_update/${id}`;
    return this.http.delete<DocumentOriginalStatus[]>(url);
  }  
}

