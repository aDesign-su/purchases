import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { NeedPosition, Reason } from "../model/formation-project-needs/formation-project-needs";

@Injectable({
    providedIn: 'root'
})
export class FormationNeedsService {

    constructor(private http: HttpClient) { }
    
    getReasons() { //: Observable<Reason[]> {
        return this.http.get<Reason[]>(`${environment.apiUrl}/`)
    }

    getNeedItems() {//: Observable<NeedPosition[]> {
        return this.http.get<NeedPosition[]>(`${environment.apiUrl}/`)
    }
}