import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { catchError, map, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  constructor(private http: HttpClient) { }

  /** Номенклатура НСИ */
  getWbs() {
    return this.http.get<TreeNode[]>(`${environment.apiUrl}/node/get_node_tree/`)
    .pipe(
      map((data) => {
        return data[0].data;
    }),
      catchError((error) => {
        return throwError(error)
      })
    )
    /**
    .toPromise()
    .then(res => <TreeNode[]>res);
    */
  }
}
