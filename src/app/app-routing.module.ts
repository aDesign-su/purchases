import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './general-content/home/home.component';
import { CategoryComponent } from './general-content/references/category/category.component';
import { ProjectRegistryComponent } from './general-content/project-portfolio/project-registry/project-registry.component';
import { NomenclatureNsiComponent } from './general-content/references/nomenclature-nsi/nomenclature-nsi.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SbnComponent } from './general-content/references/sbn/sbn.component';
import { SpnComponent } from './general-content/references/spn/spn.component';
import { WbsComponent } from './general-content/project/wbs/wbs.component';
import { CriticalityDeliveryComponent } from './general-content/references/criticality-delivery/criticality-delivery.component';
import { PlaceDeliveryComponent } from './general-content/references/place-delivery/place-delivery.component';
import { DbnComponent } from './general-content/references/dbn/dbn.component';
import { DocumentOriginalComponent } from './general-content/references/document-original/document-original.component';
import { ApplicationStatusComponent } from './general-content/references/application-status/application-status.component';
import { DocumentOriginalStatusComponent } from './general-content/references/document-original-status/document-original-status.component';
import { FormationProjectNeedsComponent } from './general-content/formation-needs/formation-project-needs/formation-project-needs.component';
import { FileDownloadingComponent } from './general-content/formation-needs/file-downloading/file-downloading.component';
import { BasicNeedsComponent } from './general-content/formation-needs/basic-needs/basic-needs.component';
import { NeedsCardComponent } from './general-content/formation-of-need/needs-card/needs-card.component';

const routes: Routes = [
  { path: '', component: SignInComponent },
  { path: 'home', component: HomeComponent },
  /** Портфель проектов */
  { path: 'project-register', component: ProjectRegistryComponent },
  /** Проект */
  { path: 'wbs', component: WbsComponent, },
  /** Формирование потребности */
  { path: 'formation-project-needs', component: FormationProjectNeedsComponent},
  { path: 'file-downloading', component: FileDownloadingComponent},
  { path: 'basic-needs/:id', component: BasicNeedsComponent},
  { path: 'register-needs-cards/:id', component: NeedsCardComponent },
  /** Справочники */
  { path: 'category', component: CategoryComponent },
  { path: 'nomenclature-nsi', component: NomenclatureNsiComponent },
  { path: 'sbn', component: SbnComponent },
  { path: 'spn', component: SpnComponent },
  { path: 'tdbn', component: DbnComponent },
  { path: 'criticality-delivery', component: CriticalityDeliveryComponent },
  { path: 'place-delivery', component: PlaceDeliveryComponent },
  { path: 'document-original', component: DocumentOriginalComponent },
  { path: 'application-status', component: ApplicationStatusComponent },
  { path: 'document-original-status', component: DocumentOriginalStatusComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
