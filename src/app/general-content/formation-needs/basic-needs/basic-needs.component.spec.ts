import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicNeedsComponent } from './basic-needs.component';

describe('BasicNeedsComponent', () => {
  let component: BasicNeedsComponent;
  let fixture: ComponentFixture<BasicNeedsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BasicNeedsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BasicNeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
