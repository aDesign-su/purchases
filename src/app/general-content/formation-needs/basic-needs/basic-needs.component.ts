import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { FormationNeedsService } from 'src/app/api-services/formation-needs.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { addNeedBasis, addNeedPosition, DialogStates, NeedForm, NeedPosition, Reason, ReasonForm, Responsible, Status } from 'src/app/model/formation-project-needs/formation-project-needs';
import { FormationProjectNeedsApiService } from '../formation-project-needs/formation-project-needs.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-basic-needs',
  templateUrl: './basic-needs.component.html',
  styleUrls: ['./basic-needs.component.scss']
})
export class BasicNeedsComponent implements OnInit {
  foundation$: BehaviorSubject<Reason | null> = new BehaviorSubject<Reason | null>(null);
  needs: BehaviorSubject<NeedPosition[]> = new BehaviorSubject<NeedPosition[]>([]);

  readonly dialogStateSubj = new BehaviorSubject<DialogStates>(DialogStates.CustomizeColumnForm);
  public dialogState$ = this.dialogStateSubj.asObservable()

  reasonDocumentTypes: any
  positionDocumentTypes: any
  documentNamesCodes = ["Код №1, Документ №1", "Код №2, Документ №2", "Код №3, Документ №3"]
  reasonStatuses!: Status[]
  positionStatuses!: Status[]
  responsibles!: Responsible[]

  theaderReason = ['№ П.П.','КОД ОП','ОСНОВАНИЕ ПОТРЕБНОСТИ','ТИП ДОКУМЕНТА ОСНОВАНИЯ','ДОКУМЕНТ','ДАТА СОЗДАНИЯ ДОКУМЕНТА','СТАТУС', 'РЕВИЗИЯ', 'ОТВЕТСТВЕННЫЙ', 'КОММЕНТАРИЙ', 'ДЕЙСТВИЯ']
  theaderNeed  = ['№ П.П.','КОД ПОЗИЦИИ ПОТРЕБНОСТИ','НАИМЕНОВАНИЕ ПОЗИЦИИ ПОТРЕБНОСТИ','КОД СДР','ТИП ПОТРЕБНОСТИ','СТАТУС','ОСНОВАНИЕ ПОТРЕБНОСТИ','КОД НСИ','КОЛ-ВО','ЕД.ИЗМ.','ДОКУМЕНТЫ ПОЗИЦИИ']

  dialogs = new Map<DialogStates, string>([
    [DialogStates.AddNeedPosition, 'Добавить позицию потребности'],
    [DialogStates.EditNeedBasis, 'Редактировать основание потребности'],
    [DialogStates.EditNeedPosition, 'Редактировать позицию потребности'],
    [DialogStates.CustomizeColumnForm, 'Настройка отображения колонок']
  ])

  needPositionForm: FormGroup;
  needBasisForm: FormGroup;

  constructor(public router: Router, public menu: MenuService, public dialog: DialogService, public api: FormationProjectNeedsApiService) {
    this.needPositionForm = new FormGroup({
      "id": new FormControl(null),
      "note": new FormControl(''),
      "title": new FormControl('', Validators.required),
      "status": new FormControl(null),
      "type_need_position": new FormControl(null),
      "responsible": new FormControl(null),
    })
    this.needBasisForm = new FormGroup({
      "id": new FormControl(null),
      "note": new FormControl(''),
      "title": new FormControl('', Validators.required),
      "status": new FormControl(null),
      "type_document": new FormControl(null),
      "responsible": new FormControl(null),
      "file_original": new FormControl(null),
    });
  }

  ngOnInit() {
    this.foundation$.next(this.router.lastSuccessfulNavigation?.extras.state as Reason)
    this.getNeedsByReason(this.foundation$.value!.id).subscribe(value => this.needs.next(value))
    this.api.getReasonStatuses().subscribe(statuses => this.reasonStatuses = statuses.results)
    this.api.getPositionStatuses().subscribe(statuses => this.positionStatuses = statuses.results)
    this.api.getReasonDocumentTypes().subscribe(types => this.reasonDocumentTypes = types.results)
    this.api.getPositionDocumentTypes().subscribe(types => this.positionDocumentTypes = types.results)
    this.api.getResponsibles().subscribe(responsibles => this.responsibles = responsibles.results)
  }

  addNeedPosition(addedNeed: any): void {
    this.api.addNeedPosition(addedNeed, this.foundation$.value!.id).subscribe(() => this.getNeedsByReason(this.foundation$.value!.id).subscribe(need => {
      return this.needs.next(need)
    }))
  }

  openAddNeedPositionDialog(): void {
    this.needPositionForm.reset()
    this.dialogStateSubj.next(DialogStates.AddNeedPosition);
    this.openDialog()
  }

  openEditNeedPositionDialog(reason: NeedPosition): void {
    this.fillForm(this.needPositionForm, reason)
    this.dialogStateSubj.next(DialogStates.EditNeedPosition);
    this.openDialog()
  }

  openEditNeedBasisDialog(reason: NeedPosition): void {
    this.fillForm(this.needBasisForm, reason)
    this.dialogStateSubj.next(DialogStates.EditNeedBasis);
    this.openDialog()
  }

  fillForm(form: FormGroup, object: NeedPosition) {
    form.patchValue(object)
    this.curStatus = this.reasonStatuses[this.needBasisForm.value.status - 1]?.id ?? null
    this.curResp = this.responsibles[this.needBasisForm.value.responsible - 1]?.id ?? null
    this.curDoc = this.reasonDocumentTypes[this.needBasisForm.value.type_document - 1]?.id ?? null
  }

  openDialog(): void {
    this.dialog.isOpen()
  }

  getNeedsByReason(id: number): Observable<any> {
    return this.api.getNeedPositionsByReason(id).pipe(map(resp => resp.results))
  }

  editPosition(foundation: NeedPosition) {

  }

  editNeedBasis(editedReason: Reason) {
    this.api.editReason(editedReason.id, editedReason).subscribe(() => this.api.getReasons().subscribe(register => this.foundation$.next(register.results.filter(value => value.id === this.foundation$.value!.id)[0])))
  }

  editNeedPosition(editedNeed: NeedForm) {
    this.api.editNeedPosition(editedNeed.id, editedNeed).subscribe(() => this.getNeedsByReason(this.foundation$.value!.id).subscribe(register => this.needs.next(register)))
  }

  deleteNeedPosition(deletedNeedId: number) {
    this.api.deleteNeedPosition(deletedNeedId).subscribe(() => this.getNeedsByReason(this.foundation$.value!.id).subscribe(register => this.needs.next(register)))
  }

  curStatus: any
  curResp: any
  curDoc: any

  filenameErr = false

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];

    if (file) {
      this.needBasisForm.patchValue({ "file_original": file });
      if (!file.name.match(/^[^А-Яа-я]*$/)) {
        this.filenameErr = true
      } else {
        this.filenameErr = false
      }
    }
  }
}
