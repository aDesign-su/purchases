import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileDownloadingComponent } from './file-downloading.component';

describe('FileDownloadingComponent', () => {
  let component: FileDownloadingComponent;
  let fixture: ComponentFixture<FileDownloadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FileDownloadingComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FileDownloadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
