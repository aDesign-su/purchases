import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasonsRegisterTableComponent } from './reasons-register-table.component';

describe('ReasonsRegisterTableComponent', () => {
  let component: ReasonsRegisterTableComponent;
  let fixture: ComponentFixture<ReasonsRegisterTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReasonsRegisterTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ReasonsRegisterTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
