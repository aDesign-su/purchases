import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Reason } from 'src/app/model/formation-project-needs/formation-project-needs';

@Component({
  selector: 'app-reasons-register-table',
  templateUrl: './reasons-register-table.component.html',
  styleUrl: './reasons-register-table.component.scss'
})
export class ReasonsRegisterTableComponent {
  theader = ['№ П.П.','КОД ОП','ОСНОВАНИЕ ПОТРЕБНОСТИ','ТИП ДОКУМЕНТА ОСНОВАНИЯ','ДОКУМЕНТ','ДАТА СОЗДАНИЯ ДОКУМЕНТА','СТАТУС', 'РЕВИЗИЯ', 'ОТВЕТСТВЕННЫЙ', 'КОММЕНТАРИЙ', 'ДЕЙСТВИЯ']
  @Input() dataSource!: Observable<Reason[]>;
  @Output() onDelete = new EventEmitter<number>()
  @Output() onEdit = new EventEmitter<Reason>()

  constructor(public router: Router) {}

  deleteFoundation(foundation: Reason) {
    this.onDelete.emit(foundation.id)
  }

  editReason(foundation: Reason) {
    this.onEdit.emit(foundation)
  }

  goToReasonCard(foundation: Reason) {
    this.router.navigate([`/basic-needs/${foundation.id}`], { state: foundation })
  }
}
