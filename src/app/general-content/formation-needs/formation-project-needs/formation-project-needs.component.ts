import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { DialogStates, NeedForm, NeedPosition, Reason, ReasonForm, Responsible, Status, addNeedBasis, addNeedPosition, customizeColumnForm } from 'src/app/model/formation-project-needs/formation-project-needs';
import { FormationProjectNeedsApiService } from './formation-project-needs.service';

@Component({
  selector: 'app-formation-project-needs',
  templateUrl: './formation-project-needs.component.html',
  styleUrls: ['./formation-project-needs.component.scss']
})
export class FormationProjectNeedsComponent implements OnInit {
  @ViewChild('fileInput') fileInput!: ElementRef;

  readonly dialogStateSubj = new BehaviorSubject<DialogStates>(DialogStates.CustomizeColumnForm);
  public dialogState$ = this.dialogStateSubj.asObservable()

  dialogs = new Map<DialogStates, string>([
    [DialogStates.AddNeedPosition, 'Добавить позицию потребности'],
    [DialogStates.EditNeedPosition, 'Редактировать позицию потребности'],
    [DialogStates.AddNeedBasis, 'Добавить основание потребности'],
    [DialogStates.EditNeedBasis, 'Редактировать основание потребности'],
    [DialogStates.CustomizeColumnForm, 'Настройка отображения колонок']
  ])

  customizeColumnForm: FormGroup = new FormGroup({});
  needPosition: FormGroup = new FormGroup({});
  reasonForm: FormGroup

  needPositions: BehaviorSubject<NeedPosition[]> = new BehaviorSubject<NeedPosition[]>([]);
  reasons: BehaviorSubject<Reason[]> = new BehaviorSubject<Reason[]>([]);

  reasonDocumentTypes: any
  positionDocumentTypes: any
  reasonStatuses!: Status[]
  positionStatuses!: Status[]
  documentNamesCodes = ["Код №1, Документ №1", "Код №2, Документ №2", "Код №3, Документ №3"]
  responsibles!: Responsible[]

  msgs = ['Поле не может быть пустым']

  constructor(public menu: MenuService, public dialog: DialogService, private router: Router, private api: FormationProjectNeedsApiService) {
    this.customizeColumnForm = new FormGroup(customizeColumnForm)
    this.needPosition = new FormGroup({
      "id": new FormControl(null),
      "note": new FormControl(''),
      "title": new FormControl('', Validators.required),
      "status": new FormControl(null),
      "type_need_position": new FormControl(null),
      "responsible": new FormControl(null),
    })
    this.reasonForm = new FormGroup({
      "id": new FormControl(null),
      "note": new FormControl(''),
      "title": new FormControl('', Validators.required),
      "status": new FormControl(null),
      "type_document": new FormControl(null),
      "responsible": new FormControl(null),
      "file_original": new FormControl(null),
    });
  }

  ngOnInit(): void {
    this.getNeedPositions().subscribe(item => this.needPositions.next(item))
    this.getReasons().subscribe(register => this.reasons.next(register))
    this.api.getReasonStatuses().subscribe(statuses => this.reasonStatuses = statuses.results)
    this.api.getPositionStatuses().subscribe(statuses => this.positionStatuses = statuses.results)
    this.api.getReasonDocumentTypes().subscribe(types => this.reasonDocumentTypes = types.results)
    this.api.getPositionDocumentTypes().subscribe(types => this.positionDocumentTypes = types.results)
    this.api.getResponsibles().subscribe(responsibles => this.responsibles = responsibles.results)
  }

  getNeedPositions(): Observable<NeedPosition[]> {
    return this.api.getNeedPositions().pipe(map(needResp => needResp.results))
  }

  openAddNeedBasisDialog(): void {
    this.dialogStateSubj.next(DialogStates.AddNeedBasis);
    this.openDialog()
  }

  openEditNeedBasisDialog(reason: Reason): void {
    this.fillForm(this.reasonForm, reason)
    this.dialogStateSubj.next(DialogStates.EditNeedBasis);
    this.openDialog()
  }

  openAddNeedPositionDialog(): void {
    this.dialogStateSubj.next(DialogStates.AddNeedPosition);
    this.openDialog()
  }

  openEditNeedPositionDialog(position: NeedPosition): void {
    this.fillFormPosition(this.needPosition, position)
    this.dialogStateSubj.next(DialogStates.EditNeedPosition);
    this.openDialog()
  }

  changeDialogState(newState: DialogStates): void {
    this.dialogStateSubj.next(newState);
  }

  openDialog(): void {
    this.dialog.isOpen()
  }

  goDownloading() {
    this.router.navigate(['file-downloading'])
  }

  addNeedPosition(addedReason: any): void {
    this.api.addNeedPosition(1, addedReason).subscribe(() => this.getNeedPositions().subscribe(need => {
      return this.needPositions.next(need)
    }))
  }

  editNeedPosition(editedNeed: NeedForm) {
    this.api.editNeedPosition(editedNeed.id, editedNeed).subscribe(() => this.getNeedPositions().subscribe(register => this.needPositions.next(register)))
  }

  deleteNeedPosition(deletedPositionId: number) {
    this.api.deleteNeedPosition(deletedPositionId).subscribe(() => this.getNeedPositions().subscribe(register => this.needPositions.next(register)))
  }

  getReasons(): Observable<Reason[]> {
    return this.api.getReasons().pipe(map(reasonResp => reasonResp.results))
  }

  addReason(addedReason: ReasonForm): void {
    const formData = new FormData();

    for ( let key in addedReason ) {
      if (addedReason[key as keyof ReasonForm]) {
        formData.append(key, addedReason[key as keyof ReasonForm]);
      }
    }

    this.api.addReason(formData).subscribe(() => this.getReasons().subscribe(register => {
      this.fileInput.nativeElement.value = null
      return this.reasons.next(register)
    }))
    
  }

  editReason(editedReason: ReasonForm) {
    this.api.editReason(editedReason.id, editedReason).subscribe(() => this.getReasons().subscribe(register => this.reasons.next(register)))
  }

  deleteReason(deletedReasonId: number) {
    this.api.deleteReason(deletedReasonId).subscribe(() => this.getReasons().subscribe(register => this.reasons.next(register)))
  }

  curStatus: any
  curResp: any
  curDoc: any

  fillForm(form: FormGroup, object: Reason) {
    form.patchValue(object)
    this.curStatus = this.reasonStatuses[this.reasonForm.value.status - 1]?.id ?? null
    this.curResp = this.responsibles[this.reasonForm.value.responsible - 1]?.id ?? null
    this.curDoc = this.reasonDocumentTypes[this.reasonForm.value.type_document - 1]?.id ?? null
  }

  fillFormPosition(form: FormGroup, object: NeedPosition) {
    form.patchValue(object)
    this.curStatus = this.reasonStatuses[this.reasonForm.value.status - 1]?.id ?? null
    this.curResp = this.responsibles[this.reasonForm.value.responsible - 1]?.id ?? null
    this.curDoc = this.reasonDocumentTypes[this.reasonForm.value.type_document - 1]?.id ?? null
  }

  filenameErr = false

  onFileSelected(event: any): void {
    const file: File = event.target.files[0];

    if (file) {
      this.reasonForm.patchValue({ "file_original": file });
      if (!file.name.match(/^[^А-Яа-я]*$/)) {
        this.filenameErr = true
      } else {
        this.filenameErr = false
      }
    }
  }
}
