import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormationProjectNeedsComponent } from './formation-project-needs.component';

describe('FormationProjectNeedsComponent', () => {
  let component: FormationProjectNeedsComponent;
  let fixture: ComponentFixture<FormationProjectNeedsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormationProjectNeedsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormationProjectNeedsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
