import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "src/environments/environment";
import { Observable, of } from 'rxjs';
import { NeedsResp, ReasonForm, ReasonsResp } from 'src/app/model/formation-project-needs/formation-project-needs';

@Injectable({
    providedIn: 'root'
})
export class FormationProjectNeedsApiService {

    private url: string = `${environment.apiUrl}/need`

    constructor(private http: HttpClient) {}

    //Основания
    getReasons(): Observable<ReasonsResp> {
        return this.http.get<ReasonsResp>(`${this.url}/need-reason/`)
    }

    addReason(reason: any): Observable<any> {
        return this.http.post(`${this.url}/need-reason/`, reason)
    }

    editReason(reasonId: number, reason: ReasonForm): Observable<any> {
        return this.http.patch(`${this.url}/need-reason/${reasonId}/`, reason)
    }

    deleteReason(id: number): Observable<any> {
        return this.http.delete(`${this.url}/need-reason/${id}/`)
    }

    getReasonStatuses(): Observable<any> {
        return this.http.get(`${this.url}/references/status-need-reason/`)
    }

    getPositionStatuses(): Observable<any> {
        return this.http.get(`${this.url}/references/status-need-position/`)
    }

    getDeliveries(): Observable<any> {
        return this.http.get(`${this.url}/references/criticality-delivery/`)
    }

    getWorks(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/references/simple-references/type-works/`)
    }

    getDivisions(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/references/simple-references/division/`)
    }

    getDeliveryPlaces(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/references/simple-references/place-delivery/`)
    }

    getUnitMeasures(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/references/simple-references/unit-measure/`)
    }

    getReasonDocumentTypes(): Observable<any> {
        return this.http.get(`${this.url}/references/type-reason-document/`)
    }

    getPositionDocumentTypes(): Observable<any> {
        return this.http.get(`${this.url}/references/type-need-position/`)
    }

    getResponsibles(): Observable<any> {
        return this.http.get(`${environment.apiUrl}/synchronization/microservice-user/`)
    }

    //Позиции потребности
    getNeedPositions(): Observable<any> {
        return this.http.get<NeedsResp>(`${this.url}/need-positions/`)
    }

    getNeedPositionsByReason(id: number): Observable<any> {
        return this.http.get<NeedsResp>(`${this.url}/need-positions/?&need_reason_id=${id}`)
        // return of([])
    }

    addNeedPosition(need: any, reasonId: number): Observable<any> {
        return this.http.post(`${this.url}/need-positions/?&need_reason_id=${reasonId}`, need)
        // return of([])
    }

    editNeedPosition(needId: number, need: any): Observable<any> {
        return this.http.patch(`${this.url}/need-positions/${needId}/`, need)
        // return of([])
    }

    deleteNeedPosition(id: number): Observable<any> {
        return this.http.delete(`${this.url}/need-positions/${id}/`)
        // return of([])
    }

    editNeedCardGeneral(id: number, card: any) {
        return this.http.patch(`${this.url}/card-general/${id}/`, card)
    }

    editNeedCardCost(id: number, card: any) {
        return this.http.patch(`${this.url}/card-cost/${id}/`, card)
    }

}