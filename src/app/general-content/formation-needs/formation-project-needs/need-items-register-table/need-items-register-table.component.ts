import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DialogStates, NeedPosition } from 'src/app/model/formation-project-needs/formation-project-needs';

@Component({
  selector: 'app-need-items-register-table',
  templateUrl: './need-items-register-table.component.html',
  styleUrl: './need-items-register-table.component.scss'
})
export class NeedItemsRegisterTableComponent {

  @Output() onEdit= new EventEmitter<NeedPosition>();
  @Output() onDelete = new EventEmitter<number>()

  theader  = ['№ П.П.','КОД ПОЗИЦИИ ПОТРЕБНОСТИ','НАИМЕНОВАНИЕ ПОЗИЦИИ ПОТРЕБНОСТИ','КОД СДР','ТИП ПОТРЕБНОСТИ','СТАТУС','ОСНОВАНИЕ ПОТРЕБНОСТИ','КОД НСИ','КОЛ-ВО','ЕД.ИЗМ.','ДОКУМЕНТЫ ПОЗИЦИИ']

  @Input() dataSource!: Observable<NeedPosition[]>;

  constructor(public router: Router) {}

  deleteFoundation(foundation: NeedPosition) {
    this.onDelete.emit(foundation.id)
  }

  editReason(foundation: NeedPosition) {
    this.onEdit.emit(foundation)
  }

  goToNeedCard(foundation: NeedPosition) {
    this.router.navigate([`/register-needs-cards/${foundation.id}`], { state: foundation })
  }
}
