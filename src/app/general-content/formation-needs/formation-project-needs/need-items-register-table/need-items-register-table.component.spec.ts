import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedItemsRegisterTableComponent } from './need-items-register-table.component';

describe('NeedItemsRegisterTableComponent', () => {
  let component: NeedItemsRegisterTableComponent;
  let fixture: ComponentFixture<NeedItemsRegisterTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NeedItemsRegisterTableComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NeedItemsRegisterTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
