import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriticalityDeliveryComponent } from './criticality-delivery.component';

describe('CriticalityDeliveryComponent', () => {
  let component: CriticalityDeliveryComponent;
  let fixture: ComponentFixture<CriticalityDeliveryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CriticalityDeliveryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CriticalityDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
