import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { CriticalityDelivery } from 'src/app/model/references/criticality-delivery';

@Component({
  selector: 'app-criticality-delivery',
  templateUrl: './criticality-delivery.component.html',
  styleUrl: './criticality-delivery.component.scss'
})
export class CriticalityDeliveryComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.cdForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
    });
  }
  cdForm: FormGroup;
  theader!: any[];
  dataSource!: CriticalityDelivery[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDataCriticalityDelivery();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'criticality_delivery', header: 'Значение критичности поставки' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getDataCriticalityDelivery(): void {
    this.reference.getCriticalityDelivery().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addCriticalityDelivery() {
    const cd = this.cdForm.value;
    this.reference.addCriticalityDelivery(cd).subscribe(
      (response: CriticalityDelivery[]) => {
        this.getDataCriticalityDelivery()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editCriticalityDelivery(element: any) {
    this.element = element.id;

    this.cdForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  saveCriticalityDelivery() {
    if (this.element) {
      let editedObject = this.cdForm.value;

      this.reference.editCriticalityDelivery(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.cdForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDataCriticalityDelivery();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delCriticalityDelivery(element: CriticalityDelivery) {
    const id = element.id;
    this.reference.delCriticalityDelivery(id).subscribe(
      (response: CriticalityDelivery[]) => {
        this.getDataCriticalityDelivery();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
