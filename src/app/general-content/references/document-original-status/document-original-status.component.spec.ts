import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentOriginalStatusComponent } from './document-original-status.component';

describe('DocumentOriginalStatusComponent', () => {
  let component: DocumentOriginalStatusComponent;
  let fixture: ComponentFixture<DocumentOriginalStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentOriginalStatusComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DocumentOriginalStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
