import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { DocumentOriginalStatus } from 'src/app/model/references/document-original-status';

@Component({
  selector: 'app-document-original-status',
  templateUrl: './document-original-status.component.html',
  styleUrl: './document-original-status.component.scss'
})
export class DocumentOriginalStatusComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.documentOriginalStatusForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
    });
  }
  documentOriginalStatusForm: FormGroup;
  theader!: any[];
  dataSource!: DocumentOriginalStatus[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDocumentOriginalStatus();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'sbn', header: 'Статус основания потребности' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getDocumentOriginalStatus(): void {
    this.reference.getDocumentOriginalStatus().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addDocumentOriginalStatus() {
    const sbn = this.documentOriginalStatusForm.value;
    this.reference.addDocumentOriginalStatus(sbn).subscribe(
      (response: DocumentOriginalStatus[]) => {
        this.getDocumentOriginalStatus()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editDocumentOriginalStatus(element: any) {
    this.element = element.id;

    this.documentOriginalStatusForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  saveDocumentOriginalStatus() {
    if (this.element) {
      let editedObject = this.documentOriginalStatusForm.value;

      this.reference.ediDocumentOriginalStatus(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.documentOriginalStatusForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDocumentOriginalStatus();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delDocumentOriginalStatus(element: DocumentOriginalStatus) {
    const id = element.id;
    this.reference.delDocumentOriginalStatus(id).subscribe(
      (response: DocumentOriginalStatus[]) => {
        this.getDocumentOriginalStatus();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
