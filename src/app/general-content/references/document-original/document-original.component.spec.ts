import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentOriginalComponent } from './document-original.component';

describe('DocumentOriginalComponent', () => {
  let component: DocumentOriginalComponent;
  let fixture: ComponentFixture<DocumentOriginalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DocumentOriginalComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DocumentOriginalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
