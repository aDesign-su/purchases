import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { DocumentOriginal } from 'src/app/model/references/document-original';

@Component({
  selector: 'app-document-original',
  templateUrl: './document-original.component.html',
  styleUrl: './document-original.component.scss'
})
export class DocumentOriginalComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.documentOriginalForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
    });
  }
  documentOriginalForm: FormGroup;
  theader!: any[];
  dataSource!: DocumentOriginal[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDocumentOriginal();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'sbn', header: 'Значение типа документа исходных данных' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getDocumentOriginal(): void {
    this.reference.getDocumentOriginal().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addDocumentOriginal() {
    const sbn = this.documentOriginalForm.value;
    this.reference.addDocumentOriginal(sbn).subscribe(
      (response: DocumentOriginal[]) => {
        this.getDocumentOriginal()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editDocumentOriginal(element: any) {
    this.element = element.id;

    this.documentOriginalForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  saveDocumentOriginal() {
    if (this.element) {
      let editedObject = this.documentOriginalForm.value;

      this.reference.ediDtocumentOriginal(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.documentOriginalForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDocumentOriginal();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delDocumentOriginal(element: DocumentOriginal) {
    const id = element.id;
    this.reference.delDocumentOriginal(id).subscribe(
      (response: DocumentOriginal[]) => {
        this.getDocumentOriginal();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
