import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NomenclatureNsiComponent } from './nomenclature-nsi.component';

describe('NomenclatureNsiComponent', () => {
  let component: NomenclatureNsiComponent;
  let fixture: ComponentFixture<NomenclatureNsiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NomenclatureNsiComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NomenclatureNsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
