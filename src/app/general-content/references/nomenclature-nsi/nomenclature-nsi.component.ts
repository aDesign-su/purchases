import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';

@Component({
  selector: 'app-nomenclature-nsi',
  templateUrl: './nomenclature-nsi.component.html',
  styleUrl: './nomenclature-nsi.component.scss'
})
export class NomenclatureNsiComponent implements OnInit {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }
  dataSource!: TreeNode[];
  newDataSource!: TreeNode[]
  transformedData: any[] = [];
  theader!: any[];

  ngOnInit() {
    /** 
    Второй вариант
    this.reference.getNomenclatureReference().then((files) => (
      this.dataSource = files[0].data
    )); 
    */

    this.getDataNomenclature()
    this.theader = [
        // { field: 'no', header: '№ П/П' },
        { field: 'level_class', header: 'Уровень класса' },
        { field: 'name_class', header: 'Наименование класса/Наименование Изделий' },
        { field: 'note', header: 'Примечание' },
        { field: 'code_group', header: 'Код группы материалов' },
        { field: 'group_mat', header: 'Группа материалов' },
        { field: 'code_okpd', header: 'Код ОКПД2' },
        { field: 'name_okpd', header: 'Наименование ОКПД2' }
    ];
  }

  getDataNomenclature(): void {
    this.reference.getNomenclatureReference().subscribe((data) => {
        this.dataSource = data;
        this.transformData();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  transformData(): void {
    this.transformedData = this.dataSource.map((item) => this.transformItem(item));
    console.log('Transformed Data:', this.transformedData);
    this.newDataSource = this.transformedData
  }
  transformItem(item: any): any {
    let transformedItem: any = {
      data: {
        name_ru: item.name_ru,
        name_lat: item.name_lat,
        level: item.level,
        note: item.note,
        name: item.name,
      },
      children: [],
    };
    if (item.children && item.children.length > 0) {
      transformedItem.children = item.children.map((child:any) => this.transformItem(child));
    }
    return transformedItem;
  }
}
