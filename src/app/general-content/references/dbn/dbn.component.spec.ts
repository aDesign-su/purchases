import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbnComponent } from './dbn.component';

describe('DbnComponent', () => {
  let component: DbnComponent;
  let fixture: ComponentFixture<DbnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DbnComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DbnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
