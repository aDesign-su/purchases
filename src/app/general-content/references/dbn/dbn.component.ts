import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Tdbn } from 'src/app/model/references/tdbn';

@Component({
  selector: 'app-dbn',
  templateUrl: './dbn.component.html',
  styleUrl: './dbn.component.scss'
})
export class DbnComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.tdbnForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
      is_active: false,
    });
  }
  tdbnForm: FormGroup;
  theader!: any[];
  dataSource!: Tdbn[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDataTdbn();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'tdbn', header: 'Значение типа документа основания потребности' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getDataTdbn(): void {
    this.reference.getTypesDocumentsBasisNeed().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addTdbn() {
    const tdbn = this.tdbnForm.value;
    this.reference.addTypesDocumentsBasisNeed(tdbn).subscribe(
      (response: Tdbn[]) => {
        this.getDataTdbn()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editTdbn(element: any) {
    this.element = element.id;

    this.tdbnForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
      is_active: element.is_active,
    })
  }
  saveTdbn() {
    if (this.element) {
      let editedObject = this.tdbnForm.value;

      this.reference.editTypesDocumentsBasisNeed(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.tdbnForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDataTdbn();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delTdbn(element: Tdbn) {
    const id = element.id;
    this.reference.delTypesDocumentsBasisNeed(id).subscribe(
      (response: Tdbn[]) => {
        this.getDataTdbn();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
