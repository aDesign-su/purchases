import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Sbn } from 'src/app/model/references/sbn';

@Component({
  selector: 'app-sbn',
  templateUrl: './sbn.component.html',
  styleUrl: './sbn.component.scss'
})
export class SbnComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.sbnForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
    });
  }
  sbnForm: FormGroup;
  theader!: any[];
  dataSource!: Sbn[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDataSbn();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'sbn', header: 'Статус основания потребности' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'automatic_status_assignment', header: 'Автоматическое присвоение статуса позициям' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getDataSbn(): void {
    this.reference.getStatusesBasisNeed().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addSbn() {
    const sbn = this.sbnForm.value;
    this.reference.addStatusesBasisNeed(sbn).subscribe(
      (response: Sbn[]) => {
        this.getDataSbn()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editSbn(element: any) {
    this.element = element.id;

    this.sbnForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  saveSbn() {
    if (this.element) {
      let editedObject = this.sbnForm.value;

      this.reference.editStatusesBasisNeed(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.sbnForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDataSbn();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delSbn(element: Sbn) {
    const id = element.id;
    this.reference.delStatusesBasisNeed(id).subscribe(
      (response: Sbn[]) => {
        this.getDataSbn();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}

