import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SbnComponent } from './sbn.component';

describe('SbnComponent', () => {
  let component: SbnComponent;
  let fixture: ComponentFixture<SbnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SbnComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SbnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
