import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { Spn } from 'src/app/model/references/spn';

@Component({
  selector: 'app-spn',
  templateUrl: './spn.component.html',
  styleUrl: './spn.component.scss'
})
export class SpnComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.spnForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
    });
  }
  spnForm: FormGroup;
  theader!: any[];
  dataSource!: Spn[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDataSpn();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'sbn', header: 'Статус основания потребности' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'automatic_status_assignment', header: 'Автоматическое присвоение статуса позициям' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }
  getDataSpn(): void {
    this.reference.getStatusesPositionNeed().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addSbn() {
    const sbn = this.spnForm.value;
    this.reference.addStatusesPositionNeed(sbn).subscribe(
      (response: Spn[]) => {
        this.getDataSpn()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editSpn(element: any) {
    this.element = element.id;

    this.spnForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  saveSpn() {
    if (this.element) {
      let editedObject = this.spnForm.value;

      this.reference.editStatusesPositionNeed(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.spnForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDataSpn();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delSpn(element: Spn) {
    const id = element.id;
    this.reference.delStatusesPositionNeed(id).subscribe(
      (response: Spn[]) => {
        this.getDataSpn();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
