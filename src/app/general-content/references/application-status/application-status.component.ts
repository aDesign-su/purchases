import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { ApplicationStatus } from 'src/app/model/references/application-status';

@Component({
  selector: 'app-application-status',
  templateUrl: './application-status.component.html',
  styleUrl: './application-status.component.scss'
})
export class ApplicationStatusComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.applicationStatusForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
      is_auto: false,
    });
  }
  applicationStatusForm: FormGroup;
  theader!: any[];
  dataSource!: ApplicationStatus[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getApplicationStatus();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'sbn', header: 'Значение типа документа исходных данных' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'automatic_status_assignment', header: 'Автоматическое присвоение статуса позиции в заявке' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getApplicationStatus(): void {
    this.reference.getApplicationStatus().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addApplicationStatus() {
    const applicationStatus = this.applicationStatusForm.value;
    this.reference.addApplicationStatus(applicationStatus).subscribe(
      (response: ApplicationStatus[]) => {
        this.getApplicationStatus()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editApplicationStatus(element: any) {
    this.element = element.id;

    this.applicationStatusForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  saveApplicationStatus() {
    if (this.element) {
      let editedObject = this.applicationStatusForm.value;

      this.reference.ediApplicationStatus(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.applicationStatusForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getApplicationStatus();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delApplicationStatus(element: ApplicationStatus) {
    const id = element.id;
    this.reference.delApplicationStatus(id).subscribe(
      (response: ApplicationStatus[]) => {
        this.getApplicationStatus();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
