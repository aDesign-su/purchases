import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { PlaceDelivery } from 'src/app/model/references/place-delivery';

@Component({
  selector: 'app-place-delivery',
  templateUrl: './place-delivery.component.html',
  styleUrl: './place-delivery.component.scss'
})
export class PlaceDeliveryComponent {
  constructor(private reference: ReferencesService,public menu:MenuService,private toast:MessageService, public dialog: DialogService,private formBuilder: FormBuilder) { 
    this.pdForm = this.formBuilder.group({
      name: "",
      note: "",
      is_default: false,
    });
  }
  pdForm: FormGroup;
  theader!: any[];
  dataSource!: PlaceDelivery[];
  section!: string;
  element!: number

  ngOnInit() {
    this.getDataPlaceDelivery();
    this.theader = [
      { field: 'no', header: '№ П/П' },
      { field: 'pd', header: 'Место поставки' },
      { field: 'value_default', header: 'Значение по умолчанию' },
      { field: 'comments', header: 'Комментарий' },
      { field: 'actions', header: 'Действия' },
    ];
  }

  getDataPlaceDelivery(): void {
    this.reference.getPlaceDelivery().subscribe((data) => {
        this.dataSource = data;
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  addSection_1() {
    this.section = 'section-1'
  }
  addSection_2() {
    this.section = 'section-2'
  }
  addPlaceDelivery() {
    const pd = this.pdForm.value;
    this.reference.addPlaceDelivery(pd).subscribe(
      (response: PlaceDelivery[]) => {
        this.getDataPlaceDelivery()
        this.dialog.openDialog = false
        this.toast.access = 'Данные добавлены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
      },
      (error: any) => {
        this.toast.access = error
        this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
      }
    );
  }
  editPlaceDelivery(element: any) {
    this.element = element.id;

    this.pdForm.patchValue({
      name: element.name,
      note: element.note,
      is_default: element.is_default,
    })
  }
  savePlaceDelivery() {
    if (this.element) {
      let editedObject = this.pdForm.value;

      this.reference.editPlaceDelivery(this.element, editedObject).subscribe(
        (data: any) => {
          this.dialog.openDialog = false
          this.pdForm.reset();
          this.toast.access = 'Данные изменены успешно.'
          this.toast.openMessageBar(this.toast.access, 'x', 'green-bar')
          this.getDataPlaceDelivery();
        },
        (error: any) => {
          this.toast.access = error
          this.toast.openMessageBar(this.toast.access, 'x', 'red-bar')
        }
      );
    }
  }
  delPlaceDelivery(element: PlaceDelivery) {
    const id = element.id;
    this.reference.delPlaceDelivery(id).subscribe(
      (response: PlaceDelivery[]) => {
        this.getDataPlaceDelivery();
        this.toast.access = 'Данные удалены успешно.'
        this.toast.openMessageBar(this.toast.access, 'x', 'blue-bar')
      },
    );
  }
}
