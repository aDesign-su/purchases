import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, filter, map } from 'rxjs';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { DialogStates, NeedPosition, Status } from 'src/app/model/formation-project-needs/formation-project-needs';
import { FormationProjectNeedsApiService } from '../../formation-needs/formation-project-needs/formation-project-needs.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-needs-card',
  templateUrl: './needs-card.component.html',
  styleUrl: './needs-card.component.scss'
})
export class NeedsCardComponent implements OnInit{
  theader = ["Код позиции потребности", "Наименование позиции потребности", "Тип потребности", "Вид работ", "Статус позиции", "Подразделение", "Требование ИД для проектирования", "Действия"]
  theaderFirstGeneral = ["Кол-во план", "Кол-во факт", "Ед. изм.", "Место поставки", "Критичность поставки", "Дополнительные характеристики", "Изготовитель", "Комментарии", "Действия"]
  theaderSecondGeneral = ["Проект", "Направление", "Объект", "Система", "Пакет работ", "Действия"]
  theaderThirdGeneral = ["Код НСИ", "Наименование НСИ", "Код номенклатуры", "Группа номенклатуры", "Подгруппа номенклатуры", "Закрепление поставки по справочнику", "Закрепление поставки по договору"]

  theaderFirstPrice = ["Кол-во план", "Кол-во по заявке", "Кол-во факт", "Ед. изм.", "Место поставки", "Критичность поставки", "Комментарии"]
  theaderSecondPrice = ["Цена за ед. план, руб. без НДС", "Цена за ед. прогноз, руб. без НДС", "Отклонение от плановой цены", "Цена по результатам торгов, руб. без НДС", "Отклонение цены по результатам торгов", "Цена в договоре, тыс. руб. без НДС", "Согласованная цена с подрядчиком, тыс. руб. без НДС", "Фактическая цена, тыс. руб. без НДС"]
  theaderThirdPrice = ["Бюджетная стоимость плановая, руб. без НДС", "Бюджетная стоимость прогнозная, руб. без НДС", "Отклонение от плановой цены", "Стоимость по результатам торгов, руб. без НДС", "Отклонение стоимости по результатам торгов", "Стоимость в договоре, тыс. руб. без НДС", "Согласованная стоимость с подрядчиком, тыс. руб. без НДС", "Фактическая стоиомсть, тыс. руб. без НДС"]

  readonly dialogStateSubj = new BehaviorSubject<DialogStates>(DialogStates.CustomizeColumnForm);
  public dialogState$ = this.dialogStateSubj.asObservable()

  dialogs = new Map<DialogStates, string>([
    [DialogStates.EditNeedCardGeneral, 'Редактировать карточку потребности общее'],
    [DialogStates.EditNeedCardCost, 'Редактировать карточку потребности стоимость']
  ])

  tabs: Map<number, string> = new Map([
    [0, ''],
    [5, 'Стоимость']
  ])
  activeIndex: number = 0
  positionStatuses!: Status[]
  positionDocumentTypes: any
  deliveryTypes: any
  workTypes: any
  divisions: any
  placeDeliveries: any
  measureUnits: any

  needCardGeneralForm: FormGroup;
  needCardCostForm: FormGroup;

  need: BehaviorSubject<NeedPosition | null> = new BehaviorSubject<NeedPosition | null>(null);

  constructor(public router: Router, public menu: MenuService, public dialog: DialogService, private api: FormationProjectNeedsApiService) {
    this.needCardGeneralForm = new FormGroup({
      "id": new FormControl(null),
      "title": new FormControl('', Validators.required),
      "additional_characteristics": new FormControl(null),
      "quantity_plan": new FormControl(null),
      "quantity_fact": new FormControl(null),
      "necessary_initial_data": new FormControl(null),
      "type_need_position": new FormControl(null),
      "status": new FormControl(null),
      "critical_delivery": new FormControl(null),
      "type_works": new FormControl(null),
      "division": new FormControl(null),
      "place_delivery": new FormControl(null),
      "unit_measure": new FormControl(null),
    })
    this.needCardCostForm = new FormGroup({
      "id": new FormControl(null),
      "quantity_plan": new FormControl(''),
      "price_unit_plan": new FormControl(''),
      "price_unit_predict": new FormControl(null),
      "price_unit_fact": new FormControl(null),
    })
  }

  openEditNeedCardGeneralDialog(card: any): void {
    this.fillForm(this.needCardGeneralForm, card)
    this.dialogStateSubj.next(DialogStates.EditNeedCardGeneral);
    this.openDialog()
  }

  openEditNeedCardCostDialog(card: any): void {
    this.fillForm(this.needCardCostForm, card)
    this.dialogStateSubj.next(DialogStates.EditNeedCardCost);
    this.openDialog()
  }

  fillForm(form: FormGroup, object: any) {
    form.patchValue(object)
  }

  openDialog(): void {
    this.dialog.isOpen()
  }

  ngOnInit(): void {
    this.need.next(this.router.lastSuccessfulNavigation?.extras.state as NeedPosition)
    this.api.getPositionDocumentTypes().subscribe(value => this.positionDocumentTypes = value.results)
    this.api.getPositionStatuses().subscribe(value => this.positionStatuses = value.results)
    this.api.getDeliveries().subscribe(value => this.deliveryTypes = value.results)
    this.api.getWorks().subscribe(value => this.workTypes = value.results)
    this.api.getDivisions().subscribe(value => this.divisions = value.results)
    this.api.getDeliveryPlaces().subscribe(value => this.placeDeliveries = value.results)
    this.api.getUnitMeasures().subscribe(value => this.measureUnits = value.results)
  }

  editNeedCardGeneral(editedCard: any) {
    this.api.editNeedCardGeneral(editedCard.id, editedCard).subscribe(() => this.api.getNeedPositions().subscribe(register => this.need.next(register.results.filter((value: NeedPosition) => value.id === this.need.value!.id)[0])))
  }

  editNeedCardCost(editedCard: any) {
    this.api.editNeedCardCost(editedCard.id, editedCard).subscribe(() => this.api.getNeedPositions().subscribe(register => this.need.next(register.results.filter((value: NeedPosition) => value.id === this.need.value!.id)[0])))
  }

  goToReasonCard(foundationId: number) {
    this.api.getReasons().pipe(
      map(resp => resp.results)
    ).subscribe(value => {
      const foundation = value.find(val => val.id === foundationId)
      this.router.navigate([`/basic-needs/${foundationId}`], { state: foundation })
    })
  }

}
