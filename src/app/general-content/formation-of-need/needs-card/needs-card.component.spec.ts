import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedsCardComponent } from './needs-card.component';

describe('NeedsCardComponent', () => {
  let component: NeedsCardComponent;
  let fixture: ComponentFixture<NeedsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NeedsCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NeedsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
