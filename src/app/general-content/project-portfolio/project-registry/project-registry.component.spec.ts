import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRegistryComponent } from './project-registry.component';

describe('ProjectRegistryComponent', () => {
  let component: ProjectRegistryComponent;
  let fixture: ComponentFixture<ProjectRegistryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectRegistryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProjectRegistryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
