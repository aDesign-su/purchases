import { Component } from '@angular/core';
import { ProjectPortfolioService } from 'src/app/api-services/project-portfolio.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';
import { ProjectRegister } from 'src/app/model/project-portfolio/project-registry';

@Component({
  selector: 'app-project-registry',
  templateUrl: './project-registry.component.html',
  styleUrl: './project-registry.component.scss'
})
export class ProjectRegistryComponent {
  constructor(private projectPortfolio: ProjectPortfolioService,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }

  ngOnInit() {
    this.getDataProjects()
  }
  theader = ['№ П.П.','СДР','Название проекта','Действие']
  dataSource!: ProjectRegister[]
  project!: string

  getDataProjects(): void {
    // this.loading = true
    this.projectPortfolio.getProjectRegister().subscribe(
      (data) => {
        this.dataSource = data;
        // this.loading = false
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  setWbs(wbs: any) {
    if(wbs) {
      this.project = wbs.name

      let projectId = wbs.id
      projectId = localStorage.setItem('project_id', projectId);
    }
  }
}
