import { Component, OnInit } from '@angular/core';
import { MenuService } from 'src/app/helpers/menu.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(public menu:MenuService) { }

  ngOnInit() {
  }

}
