import { Component } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { ProjectService } from 'src/app/api-services/project.service';
import { ReferencesService } from 'src/app/api-services/references.service';
import { DialogService } from 'src/app/helpers/dialog.service';
import { MenuService } from 'src/app/helpers/menu.service';
import { MessageService } from 'src/app/helpers/message.service';

@Component({
  selector: 'app-wbs',
  templateUrl: './wbs.component.html',
  styleUrl: './wbs.component.scss'
})
export class WbsComponent {
  constructor(private project: ProjectService,public menu:MenuService,private toast:MessageService, public dialog: DialogService) { }
  dataSource!: TreeNode[];
  newDataSource!: TreeNode[]
  transformedData: any[] = [];
  theader!: any[];

  ngOnInit() {
    /** 
    Второй вариант
    this.reference.getNomenclatureReference().then((files) => (
      this.dataSource = files[0].data
    )); 
    */

    this.getDataNomenclature()
    this.theader = [
        // { field: 'no', header: '№ П/П' },
        { field: 'level', header: 'Уровень' },
        { field: 'name', header: 'Наименование' },
        { field: 'start_date', header: 'Дата начала' },
        { field: 'end_date', header: 'Дата окончания' },
        { field: 'date_created', header: 'Дата создания' },
        { field: 'actions', header: 'Действия' },
    ];
  }
  getDataNomenclature(): void {
    this.project.getWbs().subscribe((data) => {
        this.dataSource = data;
        this.transformData();
      },
      (error: any) => {
        console.error('Error fetching data:', error);
      });
  }
  transformData(): void {
    this.transformedData = this.dataSource.map((item) => this.transformItem(item));
    console.log('Transformed Data:', this.transformedData);
    this.newDataSource = this.transformedData
  }
  transformItem(item: any): any {
    let transformedItem: any = {
      data: {
        date_created: item.date_created,
        start_date: item.start_date,
        end_date: item.end_date,
        code_wbs: item.code_wbs,
        name: item.name,
        level: item.level,
      },
      children: [],
    };
    if (item.children && item.children.length > 0) {
      transformedItem.children = item.children.map((child:any) => this.transformItem(child));
    }
    return transformedItem;
  }
}
