import { FormControl } from "@angular/forms";

export enum DialogStates {
    CustomizeColumnForm = 'customizeColumnForm',
    AddNeedBasis = 'addNeedBasis',
    EditNeedBasis = 'editNeedBasis',
    AddNeedPosition = 'addNeedPosition',
    EditNeedPosition = 'editNeedPosition',
    EditNeedCardGeneral = 'editNeedCardGeneral',
    EditNeedCardCost = 'editNeedCardCost'
}

export interface ReasonsResp {
    date: string,
    additional: any,
    type: string,
    count: number,
    next: string,
    previous: string,
    page_size: number,
    current_page: number,
    results: Reason[]
}

export interface NeedsResp {
    date: string,
    additional: any,
    type: string,
    count: number,
    next: string,
    previous: string,
    page_size: number,
    current_page: number,
    results: NeedPosition[]
}

export interface Status {
    id: number,
    note: string,
    date_create: Date,
    date_update: Date,
    title: string,
    is_default: boolean,
    is_automatic: boolean,
    user_create: number
}

export interface Responsible {
    id: number,
    user_id: number,
    username: string,
    name_full: string,
    email: string,
    departament: string,
    post: string,
    in_system: boolean,
    is_active: boolean,
    note: string,
    date_create: Date ,
    date_update: Date,
    user_create: null
}

export interface ReasonForm {
    id: number,
    note: string,
    title: string,
    file_original: any,
    status: number,
    type_document: number,
    responsible: number
}

export interface Reason {
    id: number,
    status_title: string,
    type_document_title: string,
    responsible_name: string,
    note: string,
    date_create: Date,
    date_update: Date,
    code: string,
    part_code: number,
    title: string,
    revision: number,
    file_original: string,
    project_tree: number,
    user_create: number,
    status: number,
    type_document: number,
    responsible: number
}

export interface NeedPosition {
    id: number,
    need_reason_title: string,
    code_rbi: string,
    status_title: string,
    status_contractor_title: string,
    type_need_position_title: string,
    critical_delivery_title: string,
    responsible_name: string,
    wbs_code: string,
    type_works_title: string,
    division_title: string,
    place_delivery_title: string,
    unit_measure_title: string,
    note: string,
    date_create: Date,
    date_update: Date,
    code: string,
    part_code: number,
    title: string,
    revision: number,
    additional_characteristics: string,
    note_contractor: string,
    project_tree: number,
    quantity_plan: string,
    quantity_request: string,
    quantity_fact: string,
    quantity_warehouse_contractor: string,
    price_unit_plan: string,
    price_unit_predict: string,
    price_unit_fact: string,
    price_unit_result_auction: string,
    price_unit_contract: string,
    price_unit_agreed_contractor: string,
    date_required_delivery: null,
    date_provision_ths_plan: null,
    date_provision_ths_predict: null,
    date_provision_ths_fact: null,
    date_delivery_plan: null,
    date_delivery_predict: null,
    date_delivery_fact: null,
    number_master_plan: null,
    necessary_initial_data: false,
    estimate: null,
    memo: null,
    duration_delivery_predict: number,
    duration_delivery_fact: number,
    user_create: null,
    need_reason: number,
    type_need_position: null,
    status: null,
    status_contractor: null,
    critical_delivery: null,
    type_works: null,
    division: null,
    place_delivery: null,
    unit_measure: null,
    wbs: null,
    responsible: number,
    contract: null
}

export interface NeedForm {
    id: number,
    note: string,
    title: string,
    status: number,
    type_need_position: number,
    responsible: number
}

export const customizeColumnForm = {
    "position_code": new FormControl(),
    "position_name": new FormControl(),
    "code_wbs": new FormControl(),
    "need_type": new FormControl(),
    "need_basis": new FormControl(),
    "document_type": new FormControl(),
    "document_create_date": new FormControl(),
    "document_change_date": new FormControl(),
    "card_status": new FormControl(),
    "document_comment": new FormControl(),
    "nsi_code": new FormControl(),
    "nomenclature_nsi_name": new FormControl(),
    "nomenclature_inner_manual": new FormControl(),
    "lot": new FormControl(),
    "purchase_type": new FormControl(),
    "producer_type": new FormControl(),
    "typical_delivery_time": new FormControl(),
    "master_plan_number": new FormControl(),
    "position_documents": new FormControl(),
    "count": new FormControl(),
    "unit": new FormControl(),
    "design_source_data": new FormControl(),
    "questionnaire": new FormControl(),
}

export const addNeedPosition = {
    "name": new FormControl(),
    "needType": new FormControl(),
    "documentType": new FormControl(),
    "documentCreds": new FormControl()
}

export const addNeedBasis = {
    "name": new FormControl(),
    "documentBasisType": new FormControl(),
    "responsible": new FormControl(),
    "uploadDocument": new FormControl(),
    "comment": new FormControl()
}