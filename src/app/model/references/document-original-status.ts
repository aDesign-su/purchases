export interface DocumentOriginalStatus {
    id: number
    name: string
    note: string
    is_default?: boolean
}