export interface DocumentOriginal {
    id: number
    name: string
    note: string
    is_default?: boolean
}