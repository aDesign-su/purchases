export interface Spn {
    id: number
    name: string
    note: string
    is_default?: boolean
}