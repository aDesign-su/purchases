export interface Sbn {
    id: number
    name: string
    note: string
    is_default?: boolean
}