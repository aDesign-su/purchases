export interface CriticalityDelivery {
    id: number
    name: string
    note: string
    is_default?: boolean
}