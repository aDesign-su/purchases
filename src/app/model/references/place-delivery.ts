export interface PlaceDelivery {
    id: number
    name: string
    note: string
    is_default?: boolean
}