export interface ApplicationStatus {
    id: number
    name: string
    note: string
    is_default?: boolean
    is_auto?: boolean
}