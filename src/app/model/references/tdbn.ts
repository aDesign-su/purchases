export interface Tdbn {
    id: number
    name: string
    note: string
    is_default?: boolean
}