import { Component, OnInit } from '@angular/core';
import { MenuService } from '../helpers/menu.service';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  constructor(public menu: MenuService,private router: Router) {}

  items!: MenuItem[];
  
  ngOnInit() {
      this.items = [
        {
            label: 'Портфель проектов',
            icon: 'pi pi-briefcase',
            items: [
                {label: 'Реестр проектов', route: '/project-register'},
                {label: 'Отчёт по портфелю', route: '/project-portfolio'},
                // {label: 'Open', icon: 'pi pi-fw pi-external-link'},
                // {separator: true},
                // {label: 'Quit', icon: 'pi pi-fw pi-times'}
            ]
        },
        {
            label: 'Проект',
            icon: 'pi pi-chart-line',
            expanded: this.checkActiveState(['/wbs']),
            items: [
                {label: 'СДР', route: '/wbs'},
                {label: 'График проекта', route: '/none'},
                {label: 'Команда проекта', route: '/none'}
            ]
        },
        {
            label: 'Фомирование потребности',
            icon: 'pi pi-chart-scatter',
            items: [
                {label: 'Формирование потребности проекта', route: '/formation-project-needs'},
                {label: 'Загрузка из файла', route: '/file-downloading'},
                // {label: 'Основание потребности', route: '/basic-needs'}
            ]
        },
        {
            label: 'Планирование закупок',
            icon: 'pi pi-address-book',
            items: [
                {label: 'План закупки',route: '/none'},
                {label: 'Реестр разделительных ведомостей',route: '/none'},
                {label: 'Создать резделительную ведомость',route: '/none'},
                {label: 'Реестр планов закупок',route: '/none'},
                {label: 'Лотирование',route: '/none'},
                {label: 'Реестр лотов',route: '/none'},
            ]
        },
        {
            label: 'Проведение закупок',
            icon: 'pi pi-book',
            items: [
                {label: 'Заявки на закупку',route: '/none'},
                {label: 'Закупочная документация',route: '/none'},
                {label: 'Конкурсные процедуры',route: '/none'},
                {label: 'Статус контрактации',route: '/none'},
                {label: 'Типовые договоры',route: '/none'},
                {label: 'Реестр договоров',route: '/none'},
            ]
        },
        {
            label: 'Управление поставщиками',
            icon: 'pi pi-id-card',
            items: [
                {label: 'Реестр поставщиков',route: '/none'},
                {label: 'Предквалификация',route: '/none'},
                {label: 'Аккредитация',route: '/none'},
            ]
        },
        {
            label: 'Кабинет подрядчика',
            icon: 'pi pi-receipt',
            items: [
                {label: 'Разделительные ведомости',route: '/none'},
                {label: 'Контролируемые материалы',route: '/none'},
                {label: 'Замена ТМЦ',route: '/none'},
                {label: 'Склад подрядчика',route: '/none'},
                {label: 'Передача ТМЦ',route: '/none'},
                {label: 'Претензионная работа',route: '/none'},
                {label: 'Выкуп ТМЦ',route: '/none'},
            ]
        },
        {
            label: 'Управление договорами',
            icon: 'pi pi-pen-to-square',
            items: [
                {label: 'Управление претензиями',route: '/none'},
                {label: 'Отчет по договорам',route: '/none'},
            ]
        },
        {
            label: 'Экспедайтинг',
            icon: 'pi pi-shopping-bag',
            items: [
                {label: 'Реестр статусов заказа',route: '/none'},
                {label: 'Реестр планов мероприятий',route: '/none'},
                {label: 'Реестр статусов заказа',route: '/none'},
                {label: 'Исполнение поставки материалов',route: '/none'},
                {label: 'Исполнение поставки услуг',route: '/none'},
                {label: 'Исполнение поставки работ',route: '/none'},
                {label: 'Исполнение поставки оборудования',route: '/none'},
            ]
        },
        {
            label: 'Отчетность',
            icon: 'pi pi-clipboard',
            items: [
                {label: 'Отчет по проекту',route: '/none'},
                {label: 'Трекер закупок',route: '/none'},
                {label: 'Отчет по команде',route: '/none'},
                {label: 'Отчет по потребности',route: '/none'},
                {label: 'Отчет по разделительным веодомстям',route: '/none'},
                {label: 'Отчет по контрактации',route: '/none'},
                {label: 'Отчет по экспедайтингу',route: '/none'},
            ]
        },
        {
            label: 'Справочники',
            icon: 'pi pi-file',
            expanded: this.checkActiveState(['/nomenclature-nsi']) || this.checkActiveState(['/sbn']) || this.checkActiveState(['/spn']) || this.checkActiveState(['/criticality-delivery']) || this.checkActiveState(['/place-delivery']) || this.checkActiveState(['/tdbn']),
            items: [
                {label: 'Номенклатура НСИ',route: '/nomenclature-nsi'
                    // items: [
                    //     {label: 'Объекты аналоги', icon: 'pi pi-fw pi-save', route: '/analog-objects'},
                    // ]
                },
                {label: 'Статусы основания потребности',route: '/sbn'},
                {label: 'Статусы позиции потребности',route: '/spn'},
                {label: 'Критичность поставки',route: '/criticality-delivery'},
                {label: 'Место поставки',route: '/place-delivery'},
                {label: 'Типы документов основания потребности',route: '/tdbn'},
                {label: 'Типы документов исходных данных',route: '/document-original'},
                {label: 'Статусы заявок',route: '/application-status'},
                {label: 'Статусы документов исходных данных',route: '/document-original-status'},
                {label: 'Справочник номенклатур',route: '/none'},
                {label: 'Экспедайтинг',route: '/none'},
                {label: 'Справочник макропоказателей',route: '/none'},
                {label: 'Типовые пакеты работ',route: '/none'},
                {label: 'Правила подбора НСИ',route: '/none'},
            ]
        }
    ];
  }
  checkActiveState(givenLink:any) {
    console.log(this.router.url);
    if (this.router.url.indexOf(givenLink) === -1) {
      return false;
    } else {
      return true;
    }
  }
}
