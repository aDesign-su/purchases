import { Component } from '@angular/core';
import { AuthService } from './../../api-services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrl: './sign-in.component.scss'
})
export class SignInComponent {
  public user: any;
  hide = true;
  username!: string;
  password!: string;
  formSubmitted = false;
  messageError = '';
  passwordError = '';

  constructor(public auth: AuthService, public  router:Router) { }

  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
  }

  errorMessage = ''
  login() {
    this.formSubmitted = true;
    this.messageError = '';
    this.passwordError = '';

    if (!this.username || !this.password) {
      return;
    }

    this.auth.slogin(this.username, this.password).subscribe(
      (response) => {
        console.log(response);
        console.log(response.access);
        localStorage.setItem('access', response.access)
        // redirect to home page or some other page
      },
      (error) => {
        console.log(error);
          this.messageError = 'Неверное имя пользователя или пароль!';
      });
  }
  
 
  refreshToken() {
    this.auth.refreshToken();
  }
 
  logout() {
    this.auth.logout();
  }
}
