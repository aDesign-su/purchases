import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER } from '@angular/core';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { ReactiveFormsModule } from '@angular/forms';

import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

/** Material UI */
import { FormsModule } from '@angular/forms'
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatExpansionModule } from '@angular/material/expansion';

/** PrimeNG UI */
import { PrimeNGConfig } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { PanelMenuModule } from 'primeng/panelmenu';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { TreeTableModule } from 'primeng/treetable';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { FileUploadModule } from 'primeng/fileupload';

/** Прочее */
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './general-content/home/home.component';
import { MessageService } from './helpers/message.service';
import { MenuComponent } from './menu/menu.component';
import { ReferencesService } from './api-services/references.service';

/** Портфель проектов */
import { ProjectRegistryComponent } from './general-content/project-portfolio/project-registry/project-registry.component';

/** Проект */
import { WbsComponent } from './general-content/project/wbs/wbs.component';

/** Справочники */
import { CategoryComponent } from './general-content/references/category/category.component';
import { NomenclatureNsiComponent } from './general-content/references/nomenclature-nsi/nomenclature-nsi.component';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SbnComponent } from './general-content/references/sbn/sbn.component';
import { SpnComponent } from './general-content/references/spn/spn.component';
import { CriticalityDeliveryComponent } from './general-content/references/criticality-delivery/criticality-delivery.component';
import { PlaceDeliveryComponent } from './general-content/references/place-delivery/place-delivery.component';
import { DbnComponent } from './general-content/references/dbn/dbn.component';
import { DocumentOriginalComponent } from './general-content/references/document-original/document-original.component';
import { ApplicationStatusComponent } from './general-content/references/application-status/application-status.component';
import { DocumentOriginalStatusComponent } from './general-content/references/document-original-status/document-original-status.component';
import { BasicNeedsComponent } from './general-content/formation-needs/basic-needs/basic-needs.component';
import { FileDownloadingComponent } from './general-content/formation-needs/file-downloading/file-downloading.component';
import { FormationProjectNeedsComponent } from './general-content/formation-needs/formation-project-needs/formation-project-needs.component';
import { NeedItemsRegisterTableComponent } from './general-content/formation-needs/formation-project-needs/need-items-register-table/need-items-register-table.component';
import { ReasonsRegisterTableComponent } from './general-content/formation-needs/formation-project-needs/reasons-register-table/reasons-register-table.component';
import { NeedsCardComponent } from './general-content/formation-of-need/needs-card/needs-card.component';
import { FormPanelComponent } from './form-panel/form-panel.component';

const initializeAppFactory = (primeConfig: PrimeNGConfig) => () => {
  primeConfig.ripple = true;
};

@NgModule({
  declarations: [				
    AppComponent,
      /** Прочее */
      HeaderComponent,
      HomeComponent,
      MenuComponent,
      SignInComponent,
      FormPanelComponent,
      /** Портфель проектов */
      ProjectRegistryComponent,
      /** Проект */
      WbsComponent,
      /** Формирование потребности */
      BasicNeedsComponent,
      FileDownloadingComponent,
      FormationProjectNeedsComponent,
      NeedItemsRegisterTableComponent,
      ReasonsRegisterTableComponent,
      NeedsCardComponent,
      /** Справочники */
      CategoryComponent,
      NomenclatureNsiComponent,
      SbnComponent,
      SpnComponent,
      CriticalityDeliveryComponent,
      PlaceDeliveryComponent,
      DbnComponent,
      DocumentOriginalComponent,
      ApplicationStatusComponent,
      DocumentOriginalStatusComponent,

  ],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    /** Material UI */
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTooltipModule,
    MatExpansionModule,
    
    /** PrimeNG UI */
    ButtonModule,
    TableModule,
    TreeTableModule,
    DropdownModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    DialogModule,
    TabViewModule,
    InputTextModule,
    CalendarModule,
    PanelMenuModule,
    CheckboxModule
  ],
  providers: [
    {
       provide: APP_INITIALIZER,
       useFactory: initializeAppFactory,
       deps: [PrimeNGConfig],
       multi: true,
       
    },
    MessageService,
    ReferencesService,
    provideAnimationsAsync(),
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
